# Petopia

## Where to find the website
You can find the project [here](https://les-semi-croustillants1.gitlab.io/petopia).

## How to contribute
If you're on a Windows environment, you should first [install WSL](https://docs.microsoft.com/fr-fr/windows/wsl/install-win10).

When you're on a Linux shell or WSL, you have to execute the following commands:
```sh
cd $HOME
git clone https://gitlab.com/les-semi-croustillants1/petopia
sudo apt install nodejs
cd petopia
npm install
npm start
```

Working with SSH keys for easy push and pull:  
SSH key should be generated with `ssh-keygen`.  
Generated public ssh key (default location is `~/.ssh/id_rsa.pub`) should be added to your gitlab profile via the settings.  
Don't forget to change the origin of the project with this command:  
```sh
git remote set-url origin git@gitlab.com:les-semi-croustillants1/petopia.git
```
You can watch [this video](https://youtu.be/mNtQ55quG9M) if you encounter any issues.